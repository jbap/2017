---
title: Social Event
---

What better way to end the first day of {{ site.data.event.name }} than with an evening of
socializing and refreshments? Join us right after Lightning Talks for a chance to hang out and talk
about your favorite parts of PyGotham, or anything else you want to share.

The {{ site.data.event.name }} Social Event is brought to you by [Facebook](https://www.facebook.com/) &
[Instagram](https://www.instagram.com/). Board games provided by
[Meeple Muse](https://www.meeplemuse.com/).  Tunes brought to you by
[A-Ron the DJ](https://twitter.com/A_Ron_The_DJ).

Registration is free with your PyGotham ticket. Please
[RSVP]({{ site.data.social_event.url }}) to reserve your spot.

<ul class="sponsors row">
  {% for sponsor in site.data.social_event.sponsors %}
  <li class="col-xs-12 col-sm-6 col-md-4">
    <a href="{{ sponsor['site_url'] }}">
      <img src="{{ sponsor['logo'] }}" alt="{{ sponsor.name }}">
    </a>
  </li>
  {% endfor %}
</ul>
