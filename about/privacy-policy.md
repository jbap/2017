---
title: Privacy Policy
---

## Information we collect
We collect names, email addresses, Twitter handles, and, if you're a speaker,
sometimes phone numbers.

## What we do with this information
We take privacy very seriously. We use your contact information to send updates
and conference related info to attendees. We may use your phone number if you're
a speaker and we're trying to find you before your talk. Information may be
shared with third-party vendors conducting business on behalf of the conference.
It is not used for any other purpose, nor is it given or sold to any other
party, including other attendees or sponsors.

Feel free to contact [organizers@pygotham.org](mailto:organizers@pygotham.org)
for details involving privacy.
