---
title: "First ever PyGotham Game Night"
date: 2017-09-29 16:30:00 -0400
image: /assets/meeple-muse.png
excerpt_separator: <!--more-->
---

For the first time, PyGotham is hosting a game night!

<!--more-->

Thanks to the kind people at [Meeple Muse](https://www.meeplemuse.com),
attendees of the PyGotham Social Event on October 6th will be able to relax and
play some of the best board games around!

You can sign up for the PyGotham 2017 Social Event at
[https://pygotham2017socialevent.eventbrite.com](https://pygotham2017socialevent.eventbrite.com).

Meeple Muse will be around during the event so be sure to say hi. And check them
out on Twitter at [@MeepleMuse](https://twitter.com/MeepleMuse).
