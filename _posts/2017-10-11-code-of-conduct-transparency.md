---
title: Code of conduct transparency report
date: 2017-10-11 11:00:00 -0400
excerpt_separator: <!--more-->
---

PyGotham strives to be a friendly and welcoming environment, and we take our
code of conduct seriously.  In the spirit of transparency, we’re publishing a
summary of the reports we received this year.

<!--more-->

We appreciate those reports, and we encourage anyone who witnesses a code of
conduct violation to report it via the methods listed on
<https://2017.pygotham.org/about/code-of-conduct/>. PyGotham 2017 had four
potential code of conduct incidents reported to the organizers. Anonymized
details of these reports are below:

- An attendee made a comment that could have been misconstrued as an off-color
  joke. Conference staff determined that the comment was innocent, not intended
  to be a joke, and not in violation of our code of conduct. The attendee
  self-reported this after realizing how it could have been interpreted. We
  applaud their attention to the issue and encourage others to do the same if
  they find themselves in a future similar situation.

- An attendee made an off-color joke to a large group. When conference staff
  spoke to the accused person, they denied making or hearing the joke. Whether
  or not this person was correctly identified, staff made it clear that this
  type of joke was a violation of our code of conduct and repeat offenses would
  result in them being asked to leave the conference and not welcome back the
  following year.

- A volunteer became overwhelmed by requests made of them by other attendees
  and left the conference. We’ve reached out to the volunteer to get more
  details about the interaction but have not heard back yet. We’re working on a
  plan to make sure that volunteers’ responsibilities are more clearly defined,
  that volunteers are better trained before the conference, and that special
  requests are handled by conference staff directly.

- An attendee made an unwelcome advance toward a volunteer. By the time the
  report was made, conference events were over for the day, and the attendee
  was not seen at the conference again. This was a clear violation of our code
  of conduct, and the attendee would have been asked to leave if they were able
  to be found.
