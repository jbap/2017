---
title: "Voting Now Open"
date: 2017-07-23 10:00:00 -0400
image: /assets/i-voted.jpg
attribution: https://late.am/post/2017/07/20/pygotham-voting-open.html
excerpt_separator: <!--more-->
---

_Voting has ended._

For the first time, the PyGotham program committee is looking for you, our
potential attendees, speakers, and community, to help us shape the conference by
voting on the 195 talk proposals we've received. <!--more-->We're going to hold
open voting until August 7th, after which the Program Committee will use the
votes to inform our final selections for the conference.

## How You Can Help

We want PyGotham to reflect the interests and desires of our community, so we
ask that you share your time to help review the talk submissions. We're asking a
single, simple question for each talk: would you like to see this talk in the
final PyGotham schedule? You can give each talk either a +1 ("I would definitely
like to see this talk"), a 0 ("I have no preference on this talk"), or a -1 ("I
do not think this talk should be in PyGotham").

You can sign up for an account and begin voting at vote.pygotham.org. The talk
review web site will present you with talks in random order, omitting the ones
you have already voted on. For each talk, you will see this form:

Be sure to click "Save Vote" to make sure your vote is recorded. Once you do, a
button will appear to jump to the next proposal.

![voting form]({{ '/assets/pygotham-voting-form.png' | absolute_url }}){: .center-image }

Many thanks to [Ned Jackson Lovely](http://www.njl.us/) for sharing
[progcom](https://github.com/njl/progcom), the US PyCon talk voting app, which
we are using (with light adaptation) for PyGotham. Thanks Ned!
