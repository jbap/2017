---
title: "PyGotham 2017 has started!!"
date: 2017-10-06 08:00:00 -0400
excerpt_separator: <!--more-->
---

PyGotham 2017 has started!!

<!--more-->

Here are a few useful links to guide you through the conference.

* [Lightning Talk Signup](https://docs.google.com/spreadsheets/d/14kECjmucEM-U0mlXQiHqSDbGx-Xj8h9StCt7kR23y2Y/edit?usp=sharing)
* [Open Spaces Signup](https://docs.google.com/spreadsheets/d/1h8p_-68Jv0HgHPdAJ1AUtpfFSfjVtaHU-AD06xVjJ0s/edit?usp=sharing)
* [Schedule](/talks/schedule/)
* [Social Event](https://2017.pygotham.org/events/socialevent/)
* [Young Coders](https://2017.pygotham.org/events/youngcoders/)
