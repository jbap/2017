---
name: Christopher J. Wright
talks:
- 'PySHED: a Python framework for Streaming Heterogeneous Event Data'
---

Christopher grew up in Rockville Centre, New York. He attended Brown University where he earned a BS with honors in Chemical Physics. He worked with Prof. Shouheng Sun on electrochemical CO2 reduction and the structural dynamics of nanoparticles. After working at Brookhaven National Laboratory as a summer intern, he attended the University of South Carolina, working with Prof. Xiao-Dong Zhou on the atomic structure of solid oxide fuel cell components and earned a masters in Chemical Engineering. He joined the Billinge Group in 2016 and is currently working on analysis pipelines, data processing techniques and simulation software for PDF. In his free time Christopher develops for open source projects, plays woodwind instruments, and works out by playing squash.