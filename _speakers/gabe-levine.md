---
name: Gabe Levine
talks:
- 'Machine Music: Exploring Machine Learning By Generating Music Or How To Teach A
  Computer To Rock Out'
---

Gabe Levine is a musician (http://www.theguardian.com/music/2012/feb/03/new-band-gabriel-hounds?newsfeed=true) turned Software Engineer (https://github.com/gabelev) who cannot and will not stop thinking about the intersection of music and code. He lives in Brooklyn and works at an Aerospace company. Every once in while he plays a show in between pushing commits.