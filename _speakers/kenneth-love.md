---
name: Kenneth Love
talks:
- Those Who Care, Teach!
---

Besides teaching Python, Kenneth Love is a husband and father. He created a few Python libraries (django-braces being the most popular), worked at most levels of the web stack, and was the Creative Director of a newspaper. He likes tabletop games, activism, and dry humor.