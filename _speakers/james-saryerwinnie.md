---
name: James Saryerwinnie
talks:
- Next Level Testing Revisted
---

James Saryerwinnie is a Software Development Engineer at Amazon Web Services where he works on several python libraries and frameworks including boto3 (The AWS SDK for Python), the AWS CLI, and Chalice, a serverless framework for python.  He also maintains several open source projects.