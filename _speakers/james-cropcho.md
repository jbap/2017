---
name: James Cropcho
talks:
- Avant-Garde Regular Expressions
---

James Cropcho’s favorite member of One Direction is Liam Payne.* For both Liam and for James, it wasn’t clear to to them early in their lives what their place in the world was. They are also both former long-distance runners.
 
James Cropcho has been building software applications—and companies around those applications—for over a decade.
 
He is an adjunct professor in the Interactive Telecommunications Program (ITP) at New York University. He created the graduate course Web Development with Open Data.
 
James is an Investigative Reporters and Editors (IRE) member and has held founder, co-founder and first-hire roles at startups in a variety of industries.
 
He is the creator of the MongoDB schema analyzer Variety, which was featured on the official MongoDB blog.
 
James was a member of the two-person team which uncovered the first wide-scale breach of the secret ballot in American history, and was featured  on National Public Radio.
 
He recorded his own endorsement for Radiolab, which received daily airplay on WNYC. He furnished the source material for a Freakonomics blog post. He helped to run a daylong workshop at South by Southwest. He was profiled in a video segment for BBC News.
 
_* “I do not feel strongly about this preference and I would slightly prefer it be omitted from any published bio. I specify this with a knowing wink, only because Papercall's prompt requested it.” ―JC_