---
name: Karina Ruzinov
talks:
- Shipping secret messages through barcodes
---

Software Engineer at Warby Parker since 2013. Lives and breathes building logistics tools through Python.