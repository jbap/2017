---
name: Mark Koh
talks:
- Start Being Static with MyPy
---

Mark Koh is a Backend Engineer and self-proclaimed *Expert Data Swizzler* at Spotify&trade;.  He works on multiple python-based systems and libraries there and advocates for python whenever it makes sense.  His python experience ranges from Django applications to audio-fingerprinting libraries to dancing robots.