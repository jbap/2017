---
name: Michael Preston
talks:
- Building a rigorous, inclusive, and sustainable CSforAll movement in NYC and
  beyond
---

Michael Preston is the Executive Director of CSNYC, a nonprofit that works to
increase access to computer science in NYC public schools. CSNYC is the city’s
partner in the 10-year Computer Science for All (CS4All) initiative and leads
the national CSforAll Consortium. Prior to joining CSNYC, he led digital
learning initiatives at the NYC Department of Education and software development
projects at Columbia University’s Center for Teaching and Learning. He earned a
Ph.D. in Cognitive Science in Education at Teachers College, Columbia
University.
