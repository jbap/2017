---
name: James Bennett
talks:
- Fixing the Tech Interview
---

James is a philosopher turned web geek who fell in love with Django very early on. He picked up a commit bit while working at the Lawrence Journal-World and was Django's release manager for several years. Now, he works to improve health insurance at Clover Health, serves on Django's security team and the board of directors of the Django Software Foundation, and occasionally rants about interviewing and other issues in the culture of the tech community. He thinks you should be using Python 3.