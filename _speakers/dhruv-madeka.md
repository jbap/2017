---
name: Dhruv Madeka
talks:
- bqplot - Seamless Interactive Visualizations in the Jupyter Notebook
---

Dhruv Madeka is a Senior Machine Learning Scientist at Amazon. His current research interests focus on Deep Learning, Data Visualization and Applied Mathematics. Having graduated from the University of Michigan with a BS in Operations Research and from Boston University with an MS in Mathematical Finance, Dhruv previously worked in the Quantitative Research team at Bloomberg LP, before becoming a Senior Scientist at Amazon. Dhruv's interests lie in developing models, software and tools for users to make their data analysis experience richer.
