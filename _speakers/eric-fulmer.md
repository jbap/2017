---
name: Eric Fulmer
talks:
- 'Functions Within Functions: An Intro to Decorators and Closures'
---

I've been programming for about 8 years. For me, learning to program was a struggle, so I have a lot of empathy for anyone learning, at any level. 

For the last 3 years I've been a regular at the NYC Python and Learn Python NYC Meetup groups. I've informally helped a lot of people learn, and I love doing it. I think it'd be awesome to reach out to a larger group of people.