---
name: Mahmoud Hashemi
talks:
- A Plug for Plugins
---

Mahmoud Hashemi is a backend engineer and architect, open-source                                                                        
library maintainer, and Wikipedian. Newly building services and teams                                                                   
at shopkick, for many years he built scalable enterprise Python                                                                        
services at PayPal, where he wrote [technical essays](https://www.paypal-engineering.com/tag/python/) and presented                                                                       
[O'Reilly's Enterprise Software with Python](http://shop.oreilly.com/product/0636920047346.do). On the Wikipedia side of                                                                    
things, he's known for [Listen to Wikipedia](http://listen.hatnote.com/), [Wikipedia Social Search](http://tools.wmflabs.org/hashtags),                                                                    
[the Weeklypedia newsletter](http://weekly.hatnote.com/), the [Wikipedia IFTTT channel](https://ifttt.com/wikipedia), and much much                                                                  
more. He believes education is more than something you receive, and                                                                     
that every coder should be able to see one, do one, then teach one.