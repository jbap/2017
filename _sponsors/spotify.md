---
name: Spotify
tier: gold
site_url: https://www.spotifyjobs.com/
logo: spotify.png
---
Every day, millions of people around the world soundtrack their lives with Spotify. Our goal is to
give them the perfect music, every time they fire up the app. Spotify makes it easier to discover
new music, share music with friends, and follow your favorite artists. We’ve given the world a
viable alternative to music piracy, and helped artists reach their fans like never before. To
achieve all this, we need to be as passionate about tech as we are about music. Even by tech company
standards, we’re growing like crazy - with engineering offices in New York, San Francisco,
Gothenburg and Stockholm.
