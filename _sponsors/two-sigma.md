---
name: Two Sigma
tier: silver
site_url: https://www.twosigma.com
logo: two-sigma.png
---
We are Two Sigma. We imagine breakthroughs in investment management, insurance and related fields by
pushing the boundaries of what open source and proprietary technology can do.

In the process, we work to help real people. Through our investors, we support the retirements of
millions around the world and help fund breakthrough research, education and a wide range of
charities and foundations.

Our engineers, data scientists and modelers harness data at tremendous scale, using machine
learning, distributed computing and other technologies to build powerful predictive models.

Come build with us!
