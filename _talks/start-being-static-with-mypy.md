---
abstract: Static typing in Python is something that can help make your multi-contributor
  python projects more scalable, testable, and self-documenting.  This talk will discuss
  how to start using MyPy for gradual static typing in your code base and cover lessons
  learned from implementations at Spotify.
duration: 40
level: All
room: PennTop South
slot: 2017-10-07 14:00:00-04:00
speakers:
- Mark Koh
title: Start Being Static with MyPy
type: talk
video_url: https://youtu.be/RzkuMFlmm_c
---

Python's native dynamic typing is one of the things that makes Python so fast and simple to work in.  However, when python code bases start to become large and have many contributors, they can start to become unmaintainable and error-prone.

MyPy is a static type-checking library that you can add to your python projects to check the Type Hints proposed in [PEP 484](https://www.python.org/dev/peps/pep-0484/).  Static typing allows you to explicitly specify what types of objects your functions expect -- it looks like this:
```
from typing import Iterator

def fib(n: int) -> Iterator[int]:
    a, b = 0, 1
    while a < n:
        yield a
        a, b = b, a + b
```

MyPy is a project blessed (and being worked on) by Guido himself, and is the key to making Python robust enough for enterprise production code bases.  This talk will cover the following topics:
  - The benefits of static type checking in Python
  - A crash course on adding Type Hints and MyPy to your code base
  - A demo for adding MyPy type checking to your CI/CD pipeline
  - Learnings from adding static type checking to systems at Spotify&trade;