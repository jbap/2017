---
abstract: Learning how to code is ever more important and the more kids code, the
  better. But what can you get out of teaching kids? Why it will make you better at
  your job, and what opportunities there are in NYC, all in one talk.
duration: 25
level: All
room: Madison
slot: 2017-10-07 14:00:00-04:00
speakers:
- Tobias "Tobi" Schraink
title: Why you (!) should teach kids how to code
type: talk
video_url: https://youtu.be/xAa3ks_6UBU
---

Learning how to code is ever more important and the more kids code, the better. But what can you get out of teaching kids? Why it will make you better at your job, and what opportunities there are in NYC, all in one talk.