---
abstract: "Stream data processing is a complex and challenging process. Data is often\
  \ imprecise and murky at best. How do we reconcile these two facts? \n\nThe answer\
  \ is: Try-Monads. \n\nCome learn what a Try-Monad and see the code necessary to\
  \ implement and use one within your PySpark projects. "
duration: 25
level: Intermediate
presentation_url: https://www.slideshare.net/jordanyaker/trymonads-with-big-data-using-pyspark
room: PennTop North
slot: 2017-10-06 14:00:00-04:00
speakers:
- Jordan Yaker
title: Try-Monads With Big Data Using PySpark
type: talk
video_url: https://youtu.be/30lNjCfwtdw
---

Tired of errors killing your code when they happen? Do you often think about whether Schrödinger's cat is truly dead or alive? Monads are the answer to the woes that plague your life!

This talk will cover the following topics:

* What exactly is a Monad?
* Ok, so what's a Try-Monad?
* How will a Try-Monad help with Stream Processing?
* How do I add a Try-Monad to a PySpark application?
* How do I test my Monad-ified code?

If you're interested in writing Python Stream Processing applications that can handle errors gracefully then this talk is for you!