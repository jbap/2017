---
abstract: Technical interviewing is a mess. Companies complain that they can't find
  qualified developers, but qualified developers can't get hired! This talk will dive
  into what's gone wrong in the interviewing and hiring process, and some ideas for
  how to fix it.
duration: 40
level: All
presentation_url: http://media.b-list.org/presentations/2017/pygotham/interviewing.pdf
room: PennTop South
slot: 2017-10-07 11:15:00-04:00
speakers:
- James Bennett
- Nicole Zuckerman
title: Fixing the Tech Interview
type: talk
video_url: https://youtu.be/_vwfuLdT9Cs
---

Technical interviewing is a mess. Social media is full of stories from experienced, qualified developers who struggle to get through even initial screening at tech companies, and those same tech companies' blogs are full of stories about how they can't find enough qualified developers. Something is very obviously wrong here, but what? And what can we do about it?

Drawing from experience on both ends of the interviewing table, this talk will explore some of the underlying causes of bad interviews, challenge assumptions baked in to popular interview processes, and discuss ways to build more reliable and more humane technical interviews. This talk is for anyone involved in tech hiring who's interested in learning why current approaches so often fail, and how to fix it. No experience as an interviewer or hiring manager is required. Attendees will come away with an understanding of major systemic issues in how tech interviews are typically conducted, and a framework and specific suggestions for how to remedy these issues and build interview processes which are better and more useful for everyone involved.
