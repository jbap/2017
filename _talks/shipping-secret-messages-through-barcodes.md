---
abstract: Have you ever wondered what those seemingly arbitrary black and white lines
  on your package mean? Encoding information in plain sight is an art form and barcodes
  lend themselves to go about it completely unnoticed. Let's take a deep dive into
  printing barcodes and shipping labels using Python!
duration: 25
level: All
room: Madison
slot: 2017-10-07 10:15:00-04:00
speakers:
- Karina Ruzinov
title: Shipping secret messages through barcodes
type: talk
video_url: https://youtu.be/mGl1M6joeLo
---

Barcodes are used every day to keep track of the most important things in our lives and our businesses. We use them to track packages, to count inventory, and even to encode secret information! How these encodings work are very rarely discussed and there are even fewer libraries to help create them using Python.

Shipping labels are typically generated through a specific language which can initially seem confusing and unapproachable. I will walk you through how I've used Python to lower that barrier to entry. The width and frequency of the bars will start to seem less arbitrary and you'll learn how to interpret the secret encoded information.