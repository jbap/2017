---
abstract: bqplot is a d3-based plotting library that offers its functionality directly
  in the Jupyter Notebook in which every element of a chart is a widget that can be
  bound to a python function. This means that even a novice can use bqplot to build
  full scale web applications with 10 lines of Python code.
duration: 25
level: All
room: Madison
slot: 2017-10-06 16:55:00-04:00
speakers:
- Dhruv Madeka
title: bqplot - Seamless Interactive Visualizations in the Jupyter Notebook
type: talk
video_url: https://youtu.be/rraXF0EjRC8
---

bqplot is a Python plotting library based on d3.js that offers its functionality directly in the Jupyter Notebook, including selections, interactions, and arbitrary css customizations. In bqplot, every element of a chart is an interactive ipython widget that can be bound to a python function, which serves as the callback when an interaction takes place. The bidirectional communication between Python and JavaScript is a feature of bqplot that makes the python code aware of any interactions the user has with the visualization. This allows the rapid generation of full fledged web applications directly in the Notebook with just a few lines of Python code. We will also review some of bqplot's many new and innovative visualizations and interactions - including the MarketMap and the FastIntervalSelector and demonstrate concrete applications that leverage them to enhance a Data Science or Machine Learning workflow. 
 
The talk will also cover bqplot's seamless integration with the native Jupyter widgets - including layout and styling of web applications, as well as the integration with other widget libraries such as ipyleaflet or ipyvolume. We will also demonstrate the simple way to export bqplot charts as stand alone web applications through the embedding mechanism of the ipywidgets library.
 
Finally, we will highlight bqplot as the first plotting library to have complete integration with the new JupyterLab IDE by demonstrating dashboarding, resizing and custom integrations.