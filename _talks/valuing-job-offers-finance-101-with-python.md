---
abstract: 'We get lots of pitches to come work for various firms.


  How should we value these pitches? How does our choice today affect our lives in
  the future?


  This talk uses Python and some introductory financial theory answer these questions.'
duration: 25
level: All
room: PennTop South
slot: 2017-10-07 10:15:00-04:00
speakers:
- Aaron Hall, MBA
title: 'Valuing Job Offers - Finance 101 with Python '
type: talk
video_url: https://youtu.be/rz9q57-y9kw
---

How do we value job offers? If one firm offers X and another Y, the matter may be straightforward if everything else is equal. But that is rarely the case.

How do we value options? How do we deal with the uncertainty of vesting and company performance? Should we value perks? 

This talk will present introductory financial theory, with pure Python functions first, and the objects delegating calculations to Numpy, to answer these questions.

The speaker has an MBA, works in finance, has taught Python at Columbia and NYU, and is a former business professor and financial advisor.