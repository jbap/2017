---
abstract: 'What''s the difference between an iterable, an iterator, and a generator?  Do
  you know how to create infinitely long iterables?


  Come to this talk to learn some of Python''s slightly more advanced looping techniques.'
duration: 25
level: Beginner
presentation_url: http://treyhunner.com/loop-better/
room: Madison
slot: 2017-10-06 14:40:00-04:00
speakers:
- Trey Hunner
title: 'Loop better: a deeper look at iteration in Python'
type: talk
video_url: https://youtu.be/Wd7vcuiMhxU
---

What's the difference between an iterable, an iterator, and a generator?  Do you know how to create infinitely long iterables?
 Come to this talk to learn some of Python's slightly more advanced looping techniques.

Iterables are a very big deal in Python and they became even more important in Python 3.  There's quite a bit beyond the basics when it comes to loops and looping in Python.  Let's learn some of Python's slightly more advanced looping techniques!

In this session, we'll take a step back and learn about how looping actually works in Python.  We'll then learn about a number of Python looping techniques that you've probably overlooked.

We'll learn about the difference between sequences, iterables, and iterators.  We'll also reveal the iterator protocol that powers `for` loops in Python.

After we learn the basics, we'll learn some techniques for working with infinite infinite iterables, generators, and generator expressions.

Attendees will walk away from this session with specific actionable recommendations for refactoring their own code as well as a wealth of new topics to look deeper into after the session.