---
abstract: 'You should never do it, but it''s possible to implement an entire mergesort
  algorithm in Python from scratch using only import statements.


  By walking you through how it''s done, I''ll trick you into understanding what actually
  happens when you import in Python and teach you how to banish ImportErrors.'
duration: 25
level: All
room: PennTop South
slot: 2017-10-07 13:00:00-04:00
speakers:
- George London
title: 'import madness  # how to implement mergesort from scratch using only import
  statements'
type: talk
video_url: https://youtu.be/gB1FrBodsik
---

Recently I had trouble importing a module. So I started actually reading the documentation of the Python import
system and realized that I had been writing Python for years without understanding basics like "a module is a file"
or "importing a module executes it".

A few days later I had a terrible realization -- it's possible to implement a mergesort algorithm in Python using
only the `import` statement.

Actually making it work requires learning (and subverting) almost all of the import system's underlying machinery. 
So walking through how it's done creates an excellent (and entertaining) opportunity to shine some light on the 
   darker corners of a system that Python programmers use nearly every day but can work for years without ever really understanding.
   
This talk starts off with an explanation of the basic fundamentals of the import system, i.e.

1) What does the word "module" actually mean?
2) How does Python find the source code I'm importing?
3) What is Python actually doing when I import code?
 
From there, things get pretty weird.

The rest of the talk explains how the import system alone gives us the tools to emulate function evaluation
and how that means that we can (but shouldn't!) write algorithms using import statements instead of functions.
 
I describe the classic mergesort algorithm and then I show how to actually implement it using imports 
(under the theory that nothing helps retain knowledge like watching it applied to a trainwreck.)
  
I briefly discuss how "computation" is an abstract process that can be accomplished in more ways 
that we might intuitively realize. And then I wrap up with some practical advice on how users can avoid ImportErrors
 and a warning about the serious but non-obvious security risks of importing Python code.