---
abstract: What exactly could runtime modification and one woman's transition have
  in common?
duration: 25
level: All
room: PennTop South
slot: 2017-10-06 16:55:00-04:00
speakers:
- Piper Thunstrom
title: 'Monkey Patching My Life: Being a Trans Python Developer'
type: talk
video_url: https://youtu.be/FYcwyec7HS8
---

Transition is remarkably like monkey patching a very complex legacy system: From changing identifiers to modifying our behavior, to interacting with interfaces that aren't quite sure how to deal with the modifications.

This talk will cover my experiences transitioning quite publicly in the New York City area Python community and how those interactions can inform how we design systems: both social and software.