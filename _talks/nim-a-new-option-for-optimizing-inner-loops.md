---
abstract: For performance-critical components of a Python program, a programmer's
  first choice is often to embed a shared library written in C. Nim is an expressive,
  modern language that compiles to C and can be used to provide a speed-optimized
  core to a Python module.
duration: 25
level: Intermediate
room: PennTop South
slot: 2017-10-06 14:40:00-04:00
speakers:
- Z. D. Smith
title: 'Nim: A New Option for Optimizing Inner Loops'
type: talk
video_url: https://youtu.be/IVgNVJdizHg
---

[Nim][] is a relatively young and unknown programming language that provides an enticing mix of features to the Python programmer looking to deploy highly optimized, efficient code. It has a syntax that should be familiar to Pythonistas, including significant whitespace. It is oriented towards expressiveness and ease of composition, including a powerful macro and templating system. However, it is also fast: it's strongly typed, imperative, and compiles to C.

[Nim]: https://nim-lang.org/

Python has extremely robust support for importing shared libraries written in C as a means to optimize certain areas of an application for performance. However, C is... C. It's tricky to write, requires manual memory management and manual pointer manipulation, has no type inference... It's everything that most Python programmers write Python in order to avoid.

However, the fact that Nim compiles to C means we can write highly performant code in a modern language and then employ Python's robust and pervasive C support to call it.

In this talk we'll review a little bit of the Nim language itself, and dive into how to smoothly use it in a Python application.