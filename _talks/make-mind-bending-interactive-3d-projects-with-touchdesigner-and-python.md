---
abstract: 'TouchDesigner is a programming platform for creating real-time 2D/3D projects
  with tools like the Kinect and Oculus Rift. Fortunately for us, it uses Python as
  its built-in scripting language! I''ll give a demo of TD and leverage Python to
  generate interactive 3D media using data from Websockets.

  '
duration: 25
level: Beginner
room: Madison
slot: 2017-10-07 15:40:00-04:00
speakers:
- Suby Raman
title: Make mind-bending, interactive 3D projects with TouchDesigner and Python
type: talk
video_url: https://youtu.be/IlURHtP7svg
---

TouchDesigner is a free programming platform that makes 2D/3D programming accessible and fun to users of all levels. It easily pulls video data from tech like the Kinect, Oculus Rift, the HTC Vive, and streaming internet video. Because it uses Python, you can leverage the entire ecosystem of Python tools to push your art to the next level; everything from simple parsing of TCP messages, to transforming 3D geometry with numpy, to complex signal processing with scikit. Not only that; TouchDesigner allows Python programmers to create complex, portable user interfaces for any project.

I've been using TouchDesigner to develop interactive dance visualizers using the Kinect, and I'm excited to show what it is capable of. I'll give a high-level overview of how to start working with 2D/3D data in TouchDesigner, and I’ll demo a simple project that uses real-time data from Websockets to generate 3D media. All of you in the audience will contribute!