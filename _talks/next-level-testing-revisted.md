---
abstract: You've written your unit tests and your integration tests.  And yet, you're
  still finding bugs in your code.  In this talk we'll cover advanced types of testing
  that helps you write higher quality code with less bugs.
duration: 25
level: Intermediate
room: PennTop North
slot: 2017-10-06 15:10:00-04:00
speakers:
- James Saryerwinnie
title: Next Level Testing Revisited
type: talk
video_url: https://youtu.be/FHJyCdSAFQA
---

Unit and integration tests are great first steps towards improving the quality of your python project. Ever wonder if there’s even more you can do?   In this talk we’ll cover additional types of tests that can help improve the quality and robustness of your python projects: stateful property-based testing, generative fuzz testing, long term stability testing and advanced multithreaded testing.

This talk is more than just theory.  We'll look at specific libraries and frameworks that help you write these advanced tests.  I'll also show you real world examples of bugs these tests have found from projects that I maintain.
