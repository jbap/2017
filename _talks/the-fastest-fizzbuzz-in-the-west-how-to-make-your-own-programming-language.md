---
abstract: In this talk, you'll learn how I built DIVSPL, a tongue-in-cheek domain-specific
  language, which is particularly good for implementing FizzBuzz, and as quickly as
  possible. Along the way, you'll learn about lexers, parsers, and grammars, and in
  the end, you'll know how to build your own language.
duration: 25
level: All
room: PennTop South
slot: 2017-10-06 15:40:00-04:00
speakers:
- Dustin Ingram
title: 'The Fastest FizzBuzz in the West: How to Make Your Own Programming Language'
type: talk
video_url: https://youtu.be/DZ8kyT9oDUQ
---

In this talk, you'll learn how I built DIVSPL (Dustin Ingram's Very Special Programming Language), a tongue-in-cheek domain-specific language, which is particularly good for implementing FizzBuzz -- as quickly as possible.

We'll build DIVSPL with RPLY, an implementation of David Beazley's PLY (but with a "cooler" API) and make it compatible with RPython, a restricted subset of the Python programming language. Along the way, you'll learn about lexers, parsers, and grammars, and in the end, you'll know how to build your own language.'

This talk is for anyone who has ever wondered how computers understand programming languages, or how to create your own programming language. It will be approachable for novices who have never studied programming language design, but will offer tools and concepts that will enable experienced users to actually build their own language, if they so desire.

The audience should understand that a programming language consists of a set of symbols and patterns, and that an interpreter determines whether any given set of symbols and patterns is "valid" or "invalid". The audience will NOT need prior experience or knowledge of advanced programming language concepts.

After watching the talk, the novice users in the audience should gain an understanding of how a computer understands a programming language, including programming language concepts such as tokens, lexers, parsers and grammars.  Furthermore, experienced users should be able to implement a very simple DSL of their own using the tools discussed.