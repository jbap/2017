---
abstract: "Want to learn how web authentication works? How your login information\
  \ is transmitted from a web browser to a web server, and what happens behind the\
  \ scenes? How authentication protocols actually work?\n \nBy the end of this talk,\
  \ you\u2019ll be intimately familiar with web authentication in Python.\n"
duration: 40
level: Beginner
presentation_url: https://speakerdeck.com/rdegges/almost-everything-you-ever-wanted-to-know-about-web-authentication-in-python
room: PennTop North
slot: 2017-10-06 11:15:00-04:00
speakers:
- Randall Degges
title: Everything You Ever Wanted to Know About Web Authentication in Python
type: talk
video_url: https://youtu.be/oPIPwW7lJ0E
---

Want to learn how web authentication works? How your login information is transmitted from a web browser to a web server, and what happens from that point onwards? How authentication protocols work behind the scenes?
 
In this talk, Randall Degges, Developer Advocate at Okta, will walk you through the entire web authentication flow, covering:
 
- Credential transmission
- Cookies
- Sessions
- Databases
- Best practices
 
By the end of this talk, you’ll be intimately familiar with web authentication in Python.