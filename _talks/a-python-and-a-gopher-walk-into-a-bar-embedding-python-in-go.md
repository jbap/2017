---
abstract: "You often hear of projects porting between Python & Go, but rarely about\
  \ apps using both. The Datadog Agent is written in Go, but maintains a Python plugin\
  \ system. We\u2019ll share lessons learned about cgo, the GIL & other surprises\
  \ we found as we bridged multiple languages in a single app."
duration: 25
level: Intermediate
room: PennTop South
slot: 2017-10-06 15:10:00-04:00
speakers:
- Massimiliano Pippi
title: A Python and a Gopher walk into a bar - Embedding Python in Go
type: talk
video_url: https://youtu.be/yrEi5ezq2-c
---

You often hear of projects porting between Python and Go. The tradeoffs between the two languages are well known, and the path is well documented with success stories on either end of the journey.  But what if you want—or need—to use both languages in the same application?

Datadog’s Agent is written in Go, but has a requirement to support a large existing library of Python plugins developed in house and by our community. During the talk we will share the benefits of supporting both languages and lessons learned about how to combine them performantly. From battling cgo to dancing with the GIL as we maintained concurrency, learn from our experiences in combining multiple languages in a single application.