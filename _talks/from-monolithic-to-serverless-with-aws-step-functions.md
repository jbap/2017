---
abstract: Aside from proofs of concept, how are you realistically supposed to put
  Serverless into practice? Hear how Yelp has been moving a 10 year old codebase from
  tangled spaghetti toward serverless using AWS Step Functions (SFN). Learn SFN as
  infrastructure and how to incrementally move to Serverless.
duration: 25
level: Intermediate
presentation_url: https://www.slideshare.net/ScottTriglia/from-monolithic-to-serverless-with-amazon-step-functions
room: PennTop South
slot: 2017-10-06 10:45:00-04:00
speakers:
- Scott Triglia
title: From Monolithic to Serverless with AWS Step Functions
type: talk
video_url: https://youtu.be/UeYHJISlWgk
---

## Summary
Sometimes it seems like you can hardly go a day without hearing about how “serverless” is going to change the world of backend architecture. But aside from toy proofs of concept, how are you realistically supposed to put it into practice? Most of us work with years-old codebases that are resistant to decoupling, much less easy to transition to serverless.

Come hear how Yelp has been moving a 10 year old codebase from tangled spaghetti toward a serverless future using AWS Step Functions (SFN). You’ll gain familiarity with SFN as infrastructure, learn how it can be used to effectively disentangle complicated systems, and understand how to incrementally introduce serverless components into your monolithic application.

## Who and Why
This session is targeted at any developer interested in introducing serverless architectures, but concerned that their current systems are hopelessly complicated to integrate. The talk will be very focused on pragmatic approaches and grounded in a case study on a Yelp system which has been using these strategies to move steadily toward serverless architectures.