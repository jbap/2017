---
abstract: Decorators are very useful, but not widely understood. As functional programming
  patterns become more mainstream, understanding decorators and closures is a great
  skill to have. We'll go over how both decorators and closures work, and some useful
  decorators in Python's standard library.
duration: 25
level: Intermediate
room: Madison
slot: 2017-10-06 15:40:00-04:00
speakers:
- Eric Fulmer
title: 'Functions Within Functions: An Intro to Decorators and Closures'
type: talk
video_url: https://youtu.be/AJ9nMRSGDZ8
---

One of the greatest things about Python is its dynamic nature. This dynamicity extends to how functions work. Python can create new functions at runtime, returning them based on arguments to other functions. Functions can also store external data inside themselves to use in future calls. While all of this sounds abstract and weird, it allows for some extremely useful features: closures and decorators. Decorators allow you to use pre-existing functions to extend your own functions with new behavior, or to create an interface that other programmers can use. This talk will provide an accessible tutorial to these ideas and how to apply them, as well as provide an expanded way of thinking about how to write and use functions, which is becoming more and more common in "mainstream" languages popular in industry, like Java, JavaScript, and of course Python.