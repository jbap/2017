---
abstract: 'As data scientists we want to find hidden patterns in data and probabilistic
  modeling let''s us do just that. Edward makes probabilistic modeling easy and in
  this talk you will learn how to use Edward to fit Bayesian neural networks and other
  complex models to real data.

  (edwardlib.org)'
duration: 25
level: All
room: PennTop North
slot: 2017-10-07 15:40:00-04:00
speakers:
- Maja Rudolph
title: Edward - Probabilistic Modeling Made Easy
type: talk
video_url: https://youtu.be/ph5Z1sDKTZg
---

## Edward - Probabilistic Modeling Made Easy

Edward is a python library for probabilistic modeling and inference. It is based on tensorflow and leverages the computational graph and tools such as automatic differentiation to automate inference in probabilistic models. This means that users can skip the difficult step of deriving a custom inference algorithm and can use Edward to fit more complex probabilistic models to their data.
All they have to do is specify the probabilistic model.

### Outline of the talk

First, I will introduce the tensorflow and Edward basics that are necessary to look at a few modeling examples.
The examples we will cover include how to fit a Bayesian neural network and an embedding model to real data. 

### Goals

By introducing probabilistic modeling in Edward and giving an overview of how it can be used, I hope to encourage people to use Edward for their data science projects an/or to start contributing to the library.