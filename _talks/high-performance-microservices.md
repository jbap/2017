---
abstract: Python Microservices have many advantages, but raw performance isn't typically
  one of them.  We'll break down the sequence of events in the client-server request
  model and discuss how various architectural choices affect performance with the
  goal of minimizing total response time.
duration: 40
level: Intermediate
room: PennTop South
slot: 2017-10-06 11:15:00-04:00
speakers:
- Peter Sperl
title: High Performance Microservices
type: talk
video_url: https://youtu.be/98S5fQ9SFa4
---

Architecture plays an important role in determining the ultimate performance of a system.  Caching effectiveness, for example, is largely determined by interface design and data storage policy, while IPC overhead is strongly influenced by service deployment strategy and the method of data exchange.

The techniques and designs presented are general and not specific to any one service framework.   While many of the topics discussed apply to services and systems implemented in any language, the presented performance comparisons were run in Python and there are tips and tricks that are Python specific, such as taking liberties with the garbage collector.