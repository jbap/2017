---
abstract: Asynchronous Python can dramatically improve app performance & is gaining
  popularity among web frameworks. This talk shows how to automatically handle request
  data in your applications, eliminating the need to manually pass context objects
  to every concurrent function.
duration: 25
level: Intermediate
room: PennTop North
slot: 2017-10-06 10:45:00-04:00
speakers:
- Emanuele Palazzetti
title: Context propagation for Python concurrency models
type: talk
video_url: https://youtu.be/TMX8qcmW1fw
---

Asynchronous Python can dramatically improve application performance by allowing work during traditional waiting periods. But when asynchronous libraries and frameworks are used to build web applications, request data often needs to be manually propagated across functions. Usually these frameworks provide a clean way to attach such context to the request object itself so it can be retrieved later in the route handler.

Explicitly passing the request or context objects from function to function can be time consuming and tedious. It is also error prone, especially when the code base is big and different teams are responsible for passing context data to inner functions.

At Datadog we faced these problems when we started to support asyncio and asynchronous web frameworks in our Application Performance Monitoring (APM) service. In this talk Emanuele will go over some techniques that his team used in the Python APM client to add in-process context propagation, explaining how it fits in the wide world of python concurrency models and simplifies context propagation, while maintaining asynchronous Python performance.