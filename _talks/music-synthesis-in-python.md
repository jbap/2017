---
abstract: Python has become a one-stop-shop for everything audio - from cutting edge
  digital signal processing packages to music synthesis and music composition packages.
  The goal of this talk to provide attendees with the knowledge of the fabulous audio
  analysis and music creation tools available in Python.
duration: 25
level: All
room: PennTop North
slot: 2017-10-07 13:00:00-04:00
speakers:
- Dror Ayalon
title: Music Synthesis in Python
type: talk
video_url: https://youtu.be/ROlkhVs15AM
---

### Music Synthesis in Python
Python has become a one-stop-shop for everything audio - from cutting edge digital signal processing packages to music synthesis and music composition packages. This plethora of audio related packages allows developers and musicians to build the most creative projects and experiences in a single programming language.  
  
The goal of this talk to provide attendees with the knowledge of the fabulous audio analysis and music creation tools available in Python.  
  
### Talk Overview
The talk will start with a presentation of one of my recent projects, [Luncz](https://www.drorayalon.com/#/luncz/) (pronounces “Luntz”), a tool that analyzes live playing and generates a bass line to accompany the musician. Luncz helps the musician to stay within the musical context of the original idea and leaves enough room for this idea to be developed.
We will breakdown Luncz technical components and learn about the capabilities of the DSP packages that were used to build it and the Csound Python interface that was used to generate the playback.  
  
Followed by the project presentation, we will briefly review a variety of available Python tools and packages for music creation, music analysis, and Python interfaces to the most used digital music synthesis engines. Among these tools are pyAudio, LibROSA, pyAudio, athenaCL, Cecilia, Pyo, Soundgrain, and Csound's, Supercollider’s and ChucK’s Python APIs.  
  
### About The Speaker
Dror Ayalon is a maker, programmer, user experience designer, and a user interface designer, and was the Product Manager for a few very successful start-ups. Dror is currently a graduate student at NYU. As a lifelong music lover, he researches and develops innovative music creation tools, using music information retrieval (MIR) techniques, digital signal processing (DSP), and machine learning algorithms, that will allow musicians to compose music in a variety of new ways and formats.